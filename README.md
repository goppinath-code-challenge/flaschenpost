#  flaschenpost - Coding Challenge on 22.12.2019

## Important !!!

Here by I acknowledge that all the characters in the source codes are generated and typed by me and it is a 100% work completed by my own.

## Usage of external dependencies

None of the external dependencies are used.

## Assumptions

I assumed that the App's minimum deployment target is iOS 13 and Swift 5.1.

## Architecture

`MVC` is strictly used.

## Known issues

0. The design is tested only on iPhon 11. Custom `UICollectionViewCell` sizing is not done
1. Loading UI is not implemented.
2. Some auto-layout warnings appear.
3. No data persistence such as Core Data or SQLite.
4. No Custom UI, simply the iOS built-in UI is used.
5. No comments used, because code speaks better than words.

6. No detail view added because of the requirement `A click on a product shouldn’t do anything.`
7. The `a feature to filter out beers that cost over 2 Euro per Liter` cannot be implemented because the provided JSON  does not contain `pricePerUnit: Double` rather contains `pricePerUnit: String`. Therefore the filter is applied to `price: Double`.
