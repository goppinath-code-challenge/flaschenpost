//
//  UIView+Style.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 22.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

extension UIView {
    
    func setShadow() {
        
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.75
        layer.shadowColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
    }
    
    func setRoundedBorder(cornerRadius: CGFloat = 12) {
        
        layer.cornerRadius = cornerRadius
        layer.borderWidth = 0.5
        layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
    }
}
