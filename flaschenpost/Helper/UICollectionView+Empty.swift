//
//  UICollectionView+Empty.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 22.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    var isEmptyDataSource: Bool {
    
        guard let dataSource = dataSource else { return true }
        
        for i in 0..<dataSource.numberOfSections!(in: self) {
            
            if numberOfItems(inSection: i) > 0 {
                
                return false
            }
        }
        
        return true
    }
    
    /// Presenting nil or empty String will remove the Text.
    func presentEmptyDataSource(message: String?) {
        
        if isEmptyDataSource, let message = message, !message.isEmpty {
            
            let emptyStateLabel = UILabel(frame: frame)
            
            emptyStateLabel.text = message
            emptyStateLabel.backgroundColor = UIColor.clear
            emptyStateLabel.numberOfLines = 0
            emptyStateLabel.textColor = #colorLiteral(red: 0.4756349325, green: 0.4756467342, blue: 0.4756404161, alpha: 1)
            
            // style it as necessary
            
            emptyStateLabel.textAlignment = .center
            
            backgroundView = emptyStateLabel
        }
        else {
            
            backgroundView = nil
        }
    }
}
