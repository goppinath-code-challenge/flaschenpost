//
//  Double+Euro.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 22.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

let currencyFormatter: NumberFormatter = {
    
    let currencyFormatter = NumberFormatter()
    
    currencyFormatter.numberStyle = .currency
    currencyFormatter.currencyCode = "EUR"
    
    return currencyFormatter
}()

extension Double {

public func germanFormattedCurrencyString() -> String {
    
    return currencyFormatter.string(from: NSNumber(value: self))! // Must be a value
    }
}
