//
//  ProductViewModel.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 21.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class ProductViewModel: NSObject {
    
    static let emptyDataSourceMessage = "No data available, may be try with different filter options."
    
    private var productVariations = [Flaschenpost.ProductVariation]()
    
    private var filteredProductVariations = [Flaschenpost.ProductVariation]()
    
    lazy var imageCacheManager = ImageCacheManager()
    
    var filterSpecification: Flaschenpost.ProductVariation.FilterSpecification? {
        
        willSet {
            
            apply(filterSpecification: newValue)
            filteredProductVariations.sort(sortSpecification: self.sortSpecification)
        }
    }
    
    var sortSpecification: Flaschenpost.ProductVariation.SortSpecification = Flaschenpost.ProductVariation.SortSpecification() {
        
        willSet {
            
            filteredProductVariations.sort(sortSpecification: newValue)
        }
    }
    
    func apply(filterSpecification: Flaschenpost.ProductVariation.FilterSpecification?) {
        
        guard let filterSpecification = filterSpecification else { filteredProductVariations = productVariations; return }
        
        filteredProductVariations = productVariations.filter { $0.passing(filterSpecification: filterSpecification) }
    }
    
    func loadpProducts(completion: (() -> Void)? = nil) {
        
        Flaschenpost.getProducts { [weak self] (result) in
            
            switch result {
            case .failure(let error): print(error)
            case .success(let products): self?.productVariations = products.map { $0.productVariation }.flatMap { $0 }; self?.filteredProductVariations = self?.productVariations ?? []
            }
            
            self?.apply(filterSpecification: self?.filterSpecification)
            self?.sortSpecification = Flaschenpost.ProductVariation.SortSpecification()
            
            completion?()
        }
    }
}

// MARK:- Table View DataSource
extension ProductViewModel {
    
    func numberOfSections() -> Int {
        
        return 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        
        return filteredProductVariations.count
    }
    
    func productVariation(at: IndexPath) -> Flaschenpost.ProductVariation {
        
        return filteredProductVariations[at.row]
    }
}


// MARK:- Collection View DataSource
extension ProductViewModel {
    
    func numberOfItems(in section: Int) -> Int {
        
        return filteredProductVariations.count
    }
}

extension ProductViewModel {
    
    class ImageCacheManager {
        
        private lazy var carImageCache = NSCache<NSString, UIImage>()
        
        private func load(imageURL url: URL,  completion: @escaping (UIImage?) -> ()) {
            
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                
                if let data = data {
                    
                    if let image = UIImage(data: data) {
                
                        completion(image)
                    }
                    else {
                        
                        // TODO: What if the data cannot be converted to UIImage
                        completion(nil)
                    }
                }
                else {
                    
                    // TODO: Handle network errors
                    completion(nil)
                }
            }.resume()
        }
        
        func loadImage(for productVariations: Flaschenpost.ProductVariation, completion: @escaping (UIImage) -> ()) {
            
            if let carImage = carImageCache.object(forKey: String(productVariations.variationID) as NSString) {
                
                 completion(carImage)
            }
            else {
                
                load(imageURL: productVariations.imageURL) { [weak self] image in
                    
                    DispatchQueue.main.async {  completion(image ?? #imageLiteral(resourceName: "bottle_placeholder")) }
                    
                    if let image = image { self?.carImageCache.setObject(image, forKey: String(productVariations.variationID) as NSString) }
                }
            }
        }
    }
}
