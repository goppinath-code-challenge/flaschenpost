//
//  ProductCollectionViewCell.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 22.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    static let cellIdentifier = "ProductCollectionViewCellIdentifier"
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var shortDescriptionView: UIView!
    @IBOutlet weak var shortDescriptionLabel: UILabel!
    
    @IBOutlet weak var pricePerUnitView: UIView!
    @IBOutlet weak var pricePerUnitLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        baseView.setRoundedBorder(cornerRadius: 8)
        
        pricePerUnitView.setRoundedBorder()
    }
    
    func decorate(productVariation: Flaschenpost.ProductVariation) {
        
        pricePerUnitLabel.text = productVariation.pricePerUnitText
    }
}
