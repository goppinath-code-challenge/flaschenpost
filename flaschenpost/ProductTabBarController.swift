//
//  ProductTabBarController.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 22.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

protocol ProductDisplayable {
    
    var viewModel: ProductViewModel? { get set }
    
    func reload()
}

class ProductTabBarController: UITabBarController {

    @IBOutlet var viewModel: ProductViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        delegate = self
    
        DispatchQueue.main.async {
            
            var selectedViewController = self.selectedViewController as? ProductDisplayable
            
            selectedViewController?.viewModel = self.viewModel
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        viewModel.loadpProducts { [weak self] in
            
            DispatchQueue.main.async {
                
                if let productDisplayable = self?.selectedViewController as? ProductDisplayable {
                    
                    productDisplayable.reload()
                }
            }
        }
    }
    
    // MARK: - Actions

    @IBAction func sortBBITapped(_ sender: UIBarButtonItem) {
        
        let sortSpecification = viewModel.sortSpecification
        
        sortSpecification.toggleAscending()
        
        viewModel.sortSpecification = sortSpecification
        
        (self.selectedViewController as? ProductDisplayable)?.reload()
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.destination {
            
        case let productFilterViewController as ProductFilterViewController:
            
            productFilterViewController.popoverPresentationController?.delegate = self
            productFilterViewController.filterSpecification = viewModel.filterSpecification
            productFilterViewController.filtered = { [unowned self] filterSpecification in

                self.viewModel.filterSpecification = filterSpecification
                
                (self.selectedViewController as? ProductDisplayable)?.reload()
            }
            
        default: break
        }
    }
    

}

// MARK: - UITabBarControllerDelegate
extension ProductTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        var selectedViewController = self.selectedViewController as? ProductDisplayable
        
        selectedViewController?.viewModel = viewModel
        
        (self.selectedViewController as? ProductDisplayable)?.reload()
    }
}

// MARK: - UIPopoverPresentationControllerDelegate
extension ProductTabBarController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return .none
    }
}
