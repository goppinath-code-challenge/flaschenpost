//
//  ProductFilterViewController.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 23.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class ProductFilterViewController: UIViewController {

    @IBOutlet weak var priceSlider: UISlider!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    var filterSpecification: Flaschenpost.ProductVariation.FilterSpecification?
    
    var filtered: ((Flaschenpost.ProductVariation.FilterSpecification?) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        prepareUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        filtered?(filterSpecification)
    }
    
    private func prepareUI() {
        
        priceSlider.value = Float(filterSpecification?.price ?? 0)
        
        priceLabel.text = (filterSpecification?.price ?? 0).germanFormattedCurrencyString()
    }
    
    // MARK: - Actions
    
    @IBAction func priceSliderValueChanged(_ sender: UISlider) {
        
        if filterSpecification == nil { filterSpecification = Flaschenpost.ProductVariation.FilterSpecification() }
        
        filterSpecification?.price = Double(sender.value)
        
        priceLabel.text = (filterSpecification?.price ?? 0).germanFormattedCurrencyString()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
