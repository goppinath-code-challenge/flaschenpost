//
//  Flaschenpost+Product.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 21.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

extension Flaschenpost {
    
    struct Product: Codable {
        
        struct Variation: Codable {
            
            let id: Int
            let shortDescription: String
            let price: Double
            let unit: String
            let pricePerUnitText: String
            let imageURL: URL
            
            enum CodingKeys: String, CodingKey {
                case id, shortDescription, price, unit, pricePerUnitText
                case imageURL = "image"
            }
        }
        
        let id: Int
        let brandName: String
        let name: String
        let descriptionText: String?
        
        let variations: [Variation]
        
        enum CodingKeys: String, CodingKey {
            case id, brandName, name, descriptionText
            case variations = "articles"
        }
        
        var productVariation: [ProductVariation] {
            
            return variations.map { ProductVariation(productID: id,
                                                     brandName: brandName,
                                                     name: name,
                                                     descriptionText: descriptionText,
                                                     variationID: $0.id,
                                                     shortDescription: $0.shortDescription,
                                                     price: $0.price,
                                                     unit: $0.unit,
                                                     pricePerUnitText: $0.pricePerUnitText,
                                                     imageURL: $0.imageURL) }
        }
    }
    
    struct ProductVariation {
        
        let productID: Int
        let brandName: String
        let name: String
        let descriptionText: String?
        
        let variationID: Int
        let shortDescription: String
        let price: Double
        let unit: String
        let pricePerUnitText: String
        let imageURL: URL
        
        
    }
}

extension Flaschenpost.ProductVariation {
    
    class FilterSpecification {
        
        var price: Double = 0
    }
}

extension Flaschenpost.ProductVariation {

    func passing(filterSpecification: FilterSpecification) -> Bool {
        
        return price > filterSpecification.price
    }
}

extension Flaschenpost.ProductVariation {
    
    class SortSpecification {
        
        var ascending = true
        
        func toggleAscending() {
            
            ascending = !ascending
        }
    }
}

extension Array where Element == Flaschenpost.ProductVariation {
    
    mutating func sort(sortSpecification: Flaschenpost.ProductVariation.SortSpecification) {
        
        sortSpecification.ascending ? sort(by: { $0.price < $1.price }) : sort(by: { $0.price > $1.price })
    }
}
