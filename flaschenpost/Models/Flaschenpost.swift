//
//  Flaschenpost.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 21.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

struct Flaschenpost {
    
    enum ErrorType: Error {
        
        case unknown
    }
    
    static var url: URL {
        
        // https://flapotest.blob.core.windows.net/test/ProductData.json
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "flapotest.blob.core.windows.net"
        urlComponents.path = "/test"
        
        return urlComponents.url! // It must be there
    }
    
    enum RESTMethod: String {
        
        case getProducts = "ProductData.json"
        
        var url: URL { return Flaschenpost.url.appendingPathComponent(self.rawValue) }
    }
}

extension Flaschenpost {
    
    static func getProducts(completion: @escaping (Result<[Product], Error>) -> Void) {
        
        let urlRequest = URLRequest(url: Flaschenpost.RESTMethod.getProducts.url)
        
        URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            guard let data = data else { completion(.failure(error ?? ErrorType.unknown)); return }
            
            do {
                
                let cars = try JSONDecoder().decode([Product].self, from: data)
                
                completion(.success(cars))
            }
            catch {
                
                print(error)
                completion(.failure(error))
            }
            
        }.resume()
    }
}
