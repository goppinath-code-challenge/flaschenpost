//
//  ProductTableViewCell.swift
//  flaschenpost
//
//  Created by Goppinath Thurairajah on 21.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    static let cellIdentifier = "ProductTableViewCellIdentifier"
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var brandNameView: UIView!
    @IBOutlet weak var brandNameLabel: UILabel!
    
    @IBOutlet weak var shortDescriptionView: UIView!
    @IBOutlet weak var shortDescriptionLabel: UILabel!
    
    @IBOutlet weak var pricePerUnitView: UIView!
    @IBOutlet weak var pricePerUnitLabel: UILabel!
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        baseView.setShadow()
        baseView.layer.cornerRadius = 12
        
        brandNameView.setRoundedBorder()
        shortDescriptionView.setRoundedBorder()
        pricePerUnitView.setRoundedBorder()
        
        priceView.layer.cornerRadius = 12
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func decorate(productVariation: Flaschenpost.ProductVariation) {
        
        brandNameLabel.text = productVariation.brandName
        shortDescriptionLabel.text = productVariation.shortDescription
        pricePerUnitLabel.text = productVariation.pricePerUnitText
        
        priceLabel.text = productVariation.price.germanFormattedCurrencyString()
    }
}
